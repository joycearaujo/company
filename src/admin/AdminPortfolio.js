import React,{Component} from 'react'

import config, {storage} from './../config'

class AdminPortfolio extends Component{

    constructor(props){
        super(props)

        this.state = {
            estaGravando: false
        }

        this.gravaPortfolio = this.gravaPortfolio.bind(this)
    }

    gravaPortfolio(e){
        const itemPortfolio={
            titulo: this.titulo,
            descricao: this.descricao,
            imagem: this.imagem
        }
        this.setState({estaGravando:true})
        const arquivo = itemPortfolio.imagem.files[0]
        const {name, size, type} = arquivo


        const ref = storage.ref(name)
        ref.put(arquivo)
            .then(img =>{
                img.ref.getDownloadURL()
                    .then(downloadURL =>{
                        //console.log(downloadURL)
                        const newPortfolio ={
                            titulo: itemPortfolio.titulo.value,
                            descricao: itemPortfolio.descricao.value,
                            imagem: downloadURL
                        }
                        //console.log(newPortfolio)
                        config.push('portfolio', {
                            data: newPortfolio
                        })
                        this.setState({estaGravando:false})
                    })
            })

        e.preventDefault()
    }

    render(){
        if(this.state.estaGravando){
            return(
                <div className="container">
                    <p>
                        <span className="glyphicon glyphicon-refresh"/> Aguarde ...
                    </p>
                </div>
                )
        }
        return(
            <div>
                <h2>Portfolio - Área Administrativa</h2>
                <form onSubmit={this.gravaPortfolio}>
                    <div className="form-group">
                        <label htmlFor="titulo">Titulo</label>
                        <input type="text" className="form-control" id="titulo" aria-describedby="titulo" placeholder="Titulo" ref={(ref) => this.titulo = ref}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="descricao">Descricao</label>
                        <textarea className="form-control" id="descricao" rows="3" ref={(ref) => this.descricao = ref}></textarea>
                    </div>
                    <div className="form-group">
                        <label htmlFor="imagem">Imagem</label>
                        <input type="file" className="form-control-file" id="imagem" ref={(ref) => this.imagem = ref}/>
                    </div>
                    <button type="submit" className="btn btn-primary">Salvar</button>
                </form>
            </div>
        )
    }
}

export default AdminPortfolio