const Rebase = require('re-base')
const firebase = require('firebase')

const firebaseConfig = {
    apiKey: "AIzaSyCun2Ck-XCdV_pf6SkR5NmV8hK_ecjb5o8",
    authDomain: "teste-portfolio-4f451.firebaseapp.com",
    databaseURL: "https://teste-portfolio-4f451.firebaseio.com",
    projectId: "teste-portfolio-4f451",
    storageBucket: "teste-portfolio-4f451.appspot.com",
    messagingSenderId: "738785945503",
    appId: "1:738785945503:web:3eec8e2c6f4215a1"
  }

  const app =firebase.initializeApp(firebaseConfig)
  const config = Rebase.createClass(app.database())
  
  export const storage = app.storage()
  export const auth = app.auth()
  export default config